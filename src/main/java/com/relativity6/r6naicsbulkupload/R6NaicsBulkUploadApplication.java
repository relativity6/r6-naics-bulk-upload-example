package com.relativity6.r6naicsbulkupload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class R6NaicsBulkUploadApplication extends SpringBootServletInitializer {

    private BulkApiService bulkApiService;
    private static final Logger loggerHandler = LoggerFactory.getLogger(R6NaicsBulkUploadApplication.class);


    public static void main(String[] args) {

        loggerHandler.info("Application started");

        SpringApplication.run(R6NaicsBulkUploadApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {

        return application.sources(R6NaicsBulkUploadApplication.class);
    }

    @Bean
    public void sendFileUpload(){

        try{

            var folio = bulkApiService.uploadFile();
            var status = bulkApiService.trackingFile(folio);

            if(Boolean.TRUE.compareTo(status) == 0){

                bulkApiService.downloadFile(folio);
            }

        }catch (Exception e){

            loggerHandler.error("An error in bulk upload", e);
        }
    }


    @Autowired
    public void setBulkApiService(BulkApiService bulkApiService) {
        this.bulkApiService = bulkApiService;
    }
}

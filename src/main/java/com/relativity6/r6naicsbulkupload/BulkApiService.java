package com.relativity6.r6naicsbulkupload;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class BulkApiService {

    private static final Logger logger = LoggerFactory.getLogger(BulkApiService.class);

    private final String token = "YOUR_TOKEN_HERE";
    private final String urlStageUpload = "https://api.stage.usemarvin.ai/naics/naics/external/upload";
    private final String urlStageTracking = "https://api.stage.usemarvin.ai/naics/naics/external/upload/file/info";
    private final String urlStageDownload = "https://api.stage.usemarvin.ai/naics/naics/external/upload/file/download";
    private final String urlBetaUpload = "https://api.stage.usemarvin.ai/naics/naics/external/upload";
    private final String urlBetaTracking = "https://api.stage.usemarvin.ai/naics/naics/external/upload/file/info";
    private final String urlBetaDownload = "https://api.stage.usemarvin.ai/naics/naics/external/upload/file/download";


    public String uploadFile() throws IOException {

        HttpResponse httpresponse = null;
        String stringResponse = "";

        try{

            var file = new File("FILE_PATH");

            var description = "description example";

            CloseableHttpClient httpclient = HttpClients.createDefault();

            MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();

            entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            entityBuilder.addTextBody("token", token);
            entityBuilder.addTextBody("description", description);
            entityBuilder.addBinaryBody("file", file, ContentType.create("text/csv"), file.getName());

            HttpEntity multiPartHttpEntity = entityBuilder.build();

            RequestBuilder requestBuilder = RequestBuilder.post(urlStageUpload);

            requestBuilder.setEntity(multiPartHttpEntity);

            HttpUriRequest multipartRequest = requestBuilder.build();

            ResponseHandler<String> responseHandler=new BasicResponseHandler();
            stringResponse = httpclient.execute(multipartRequest, responseHandler);

            logger.info(stringResponse);
           //logger.info("response: " + EntityUtils.toString(httpresponse.getEntity()));
           //logger.info("status: {}", httpresponse.getStatusLine().getStatusCode());

           httpclient.close();
        }catch (Exception e){

            logger.error("An error occur while uploading file", e);
        }

        return getFolio(stringResponse);
    }

    private String getFolio(String content) {

        try{

            var objectMapperR = new ObjectMapper();

            var mapData = objectMapperR.readValue(content, LinkedHashMap.class);

            var responseMap = (Map<String, Object>) mapData.getOrDefault("response", "");

            var folio = (String) responseMap.getOrDefault("folio", "");

            return folio;
        }catch (Exception e){

            logger.error("An error occur while generating file folio", e);
        }

        return "";
    }


    public Boolean trackingFile(String folio){

        var finished = false;

        try{

            StringBuilder response = new StringBuilder();

            URL url = new URL(urlStageTracking + "?token="+ token + "&folio=" + folio);

            var iterator = 0;

            while (Boolean.FALSE.compareTo(finished) == 0){

                logger.info("==========================================================================");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");

                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;

                while ((line = rd.readLine()) != null) {

                    response.append(line);
                }

                rd.close();

                logger.info("iteration check: " + getProgressInfo(response.toString()));
                logger.info("response: {}", response);

                var status = getFileStatus(response.toString());

                if(status.compareTo("FINISHED") == 0){

                    finished = true;
                }

                response.setLength(0);
            }
        }catch (Exception e){

            logger.error("An error occur while tracking file", e);
        }

        return finished;
    }

    private String getProgressInfo(String response) {

        try{

            var objectMapperR = new ObjectMapper();

            var mapData = objectMapperR.readValue(response, LinkedHashMap.class);

            var responseMap = (Map<String, Object>) mapData.getOrDefault("response", "");

            var progressInfo = (Map<String, Object>) responseMap.getOrDefault("progressInfo", "");

            var processedRecords = (Integer) progressInfo.getOrDefault("processedRecords", 0);
            var totalRecords = (Integer) responseMap.getOrDefault("records", 0);

            return "total items : " + totalRecords + " - current item: " + processedRecords;
        }catch (Exception e){

            logger.error("An error occur while getting progress info", e);
        }

        return "";
    }

    private String getFileStatus(String response) {

        try{

            var objectMapperR = new ObjectMapper();

            var mapData = objectMapperR.readValue(response, LinkedHashMap.class);

            var responseMap = (Map<String, Object>) mapData.getOrDefault("response", "");

            var progressInfo = (Map<String, Object>) responseMap.getOrDefault("progressInfo", "");

            var status = (String) progressInfo.getOrDefault("status", "");

            return status;
        }catch (Exception e){

            logger.error("An error occur while generating file folio", e);
        }

        return "";
    }

    public void downloadFile(String folio){

        var part = 0;
        var status = "SUCCESSFUL";

        try{

            StringBuilder response = new StringBuilder();

            URL url = new URL(urlStageDownload + "?folio=" + folio + "&token=" +  token + "&part=" + part + "&fileStatus=" + status);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;

            while ((line = rd.readLine()) != null) {

                response.append(line);
            }

            rd.close();

            logger.info("response: {}", response);
        }catch (Exception e){

        }
    }
}
